# Second -hand item trading system

### deployment

* Open pom.xml to download Maven dependencies.
* Copy the 5 key files in FISCO/NODES/127.0.0.1/SDK folder to/src/Resources.
* Open the /src/resources/application.yml, modify the IP address and port of the node to ensure that the computer can connect to the FISCO-BCOS service.
* Two .sol contracts in/src/resources/Contracts are deployed to FISCO-BCOS.
* Deploy two contracts in the FISCO console, obtain the contract address, copy to the corresponding position of/src/main/java/.../secondhandmarket/util/constant.java.
*ProjectPath and ProjectBuildpath fields in Constant.java modify the absolute path of the *** absolute path of the corresponding folder in the computer.
* Run the program, open LocalHost:8000/SecondHandMarket to enter the system.