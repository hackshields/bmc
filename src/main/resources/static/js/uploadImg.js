var imgFile = []; //文件流
var imgSrc = []; //图片路径
var imgName = []; //图片名字
$(function() {
    // 鼠标经过显示删除按钮
    $('.content-img-list').on('mouseover', '.content-img-list-item', function() {
        $(this).children('div').removeClass('hide');
    });
    // 鼠标离开隐藏删除按钮
    $('.content-img-list').on('mouseleave', '.content-img-list-item', function() {
        $(this).children('div').addClass('hide');
    });
    // 单个图片删除
    $(".content-img-list").on("click", '.content-img-list-item a .gcllajitong', function() {
        var index = $(this).parent().parent().parent().index();
        imgSrc.splice(index, 1);
        imgFile.splice(index, 1);
        imgName.splice(index, 1);
        var boxId = ".content-img-list";
        addNewContent(boxId);
        if (imgSrc.length < 4) { //显示上传按钮
            $('.content-img .file').show();
        }
    });
    var boxId = ".content-img-list";
            addNewContent(boxId);

});
//图片上传
$('#upload').on('change', function(e) {
    for (i = 0; i < this.files.length; i++){
        var imgSize = this.files[i].size;
        if (imgSize > 1024 * 1000 * 1) { //1M
            return alert("Gambar yang diunggah tidak boleh melebihi 1MB");
        };
        var imgType = this.files[i].type;
        if (imgType != 'image/png' && imgType != 'image/jpeg' && imgType != 'image/gif') {
            return alert("Format unggahan gambar salah");
        }
    }
    var imgBox = '.content-img-list';
    var fileList = this.files;
    for (var i = 0; i < fileList.length; i++) {
        var imgSrcI = getObjectURL(fileList[i]);
        imgName.push(fileList[i].name);
        imgSrc.push(imgSrcI);
        imgFile.push(fileList[i]);
    }
    addNewContent(imgBox);
    this.value = null; //上传相同图片
});

//删除
function removeImg(obj, index) {
    imgSrc.splice(index, 1);
    imgFile.splice(index, 1);
    imgName.splice(index, 1);
    var boxId = ".content-img-list";
    addNewContent(boxId);
}

//图片展示
function addNewContent(obj) {
    console.log(imgSrc);
    $(obj).html("");
    if(imgSrc.length>0){
        for (var a = 0; a < imgSrc.length; a++) {
            var oldBox = $(obj).html();
            $(obj).html(oldBox + '<li class="content-img-list-item"><img src="' + imgSrc[a] + '" alt="">' +
                '<div class="hide"><a index="' + a + '" class="delete-btn"><i class="gcl gcllajitong"></i></a>' +
                '</i></a></div></li>');
        }
    }else{
        $(obj).html(oldBox + '<li class="content-img-list-item"><img src="/Users/azlan/Documents/Java/Sprint/arifini/arifin_bmc/src/main/resources/static/img/profile.jpg" alt="">' +
                        '<div class="hide"><a index="' + a + '" class="delete-btn"><i class="gcl gcllajitong"></i></a>' +
                        '</i></a></div></li>');
    }
}

//建立可存取到file的url
function getObjectURL(file) {
    var url = null;
    if (window.createObjectURL != undefined) { // basic
        url = window.createObjectURL(file);
    } else if (window.URL != undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file);
    } else if (window.webkitURL != undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file);
    }
    return url;
}