package cn.scut.wg.secondhandmarket.util;

import java.math.BigInteger;

public interface Constant {

    public static final Integer CONNECT_SECONDS = 30;
    public static final Integer CONNECT_SLEEP_PER_MILLIS = 1;
    public static final Integer TIME_OUT = 30000;

    BigInteger gasPrice = new BigInteger("300000000");
    BigInteger gasLimit = new BigInteger("300000000");

    String accountContractAddress = "0xe5ed543bd73fc82ab2cf511a327d6083f9898255";
    String tradeContractAddress = "0xa82b41079977872f742197a06459332fd14f366d";

    String projectPath = "/Users/arifin/Downloads/script/arifin_bmc/src/main/resources/static";
    String projectBuildPath = "/Users/arifin/Downloads/script/arifin_bmc/target/classes/static";

}
